<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = [

	// O
	'option_datas_choix_label'         => 'Liste des choix à classer',
	'option_datas_choix_explication'   => 'Vous devez indiquer un choix par ligne.<br>
La liste ne doit plus être modifiée par la suite sous peine de fausser les réponses précédentes.',

	// S
	'saisie_classer_choix_titre'       => 'Liste de choix à classer',
	'saisie_classer_choix_explication' => 'Permet de classer une liste de choix par ordre de préférence',
	'saisie_radio_defaut_choix1'       => 'Choix 1',
	'saisie_radio_defaut_choix2'       => 'Choix 2',
	'saisie_radio_defaut_choix3'       => 'Choix 3',
	'saisie_titre'                     => 'Saisie classer choix',
];
