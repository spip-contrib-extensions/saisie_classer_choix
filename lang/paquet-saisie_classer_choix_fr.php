<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'saisie_classer_choix_description' => '',
	'saisie_classer_choix_nom'         => 'Saisie classer choix',
	'saisie_classer_choix_slogan'      => 'Une saisie qui permet de classer des choix par ordre de préférence',
);
