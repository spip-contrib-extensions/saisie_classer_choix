<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function saisies_trier_choix_valeurs($valeur) {
	// trier les valeurs selon le classement choisi
	if(is_array($valeur) && $valeur) {
		asort($valeur);
	}
	return $valeur;
}
function saisies_trier_choix_data($data, $valeur) {
	// trier les valeurs selon le classement choisi
	$valeur = saisies_trier_choix_valeurs($valeur);
	if(is_array($valeur) && $valeur) {
		include_spip('inc/saisies_data');
		if(!is_array($data)) {
			$data = saisies_chaine2tableau($data);
		}
		$data_tri = array();
		// reconstruire data selon le classement des valeurs
		foreach ($valeur as $key => $value) {
			$data_tri[$key] = $data[$key];
		}
		return $data_tri;
	} else {
		return $data;
	}
}