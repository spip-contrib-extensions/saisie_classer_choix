<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Ajoute JS et CSS de saisie_classer_choix dans l'espace privé
 *
 * @param string $flux
 * @return string $flux modifié
 **/
function saisie_classer_choix_header_prive(string $flux): string {
	$flux .= saisie_classer_choix_generer_head();

	return $flux;
}

/**
 * Ajoute JS et CSS de saisie_classer_choix dans toutes les pages de l'espace public selon la config de saisies
 *
 * @param string $flux
 * @return string $flux modifié
 **/
function saisie_classer_choix_insert_head(string $flux): string {
	include_spip('inc/config');
	if (lire_config('saisies/assets_global')) {
		$flux .= saisie_classer_choix_generer_head();
	}
	return $flux;
}

/**
 * Ajoute JS et CSS de saisie_classer_choix si nécessaire
 *
 * @param string $flux
 * @return string $flux modifié
 **/
function saisie_classer_choix_affichage_final(string $flux): string {
	include_spip('inc/config');
	if (
		!lire_config('saisies/assets_global')
		&& $GLOBALS['html'] // si c'est bien du HTML
		&& strpos($flux, '<!--!inserer_saisie_editer-->') !== false // et qu'on a au moins une saisie
		&& strpos($flux, '<head') !== false // et qu'on a la balise <head> quelque part
	) {
		$head = saisie_classer_choix_generer_head($flux, true);
		$flux = str_replace('</head>', "$head</head>", $flux);
	}

	return $flux;
}

/**
 * Génère le contenu du head pour saisie_classer_choix (css et js)
 *
 * @param string $html_content
 * @param bool   $tester_saisies
 *
 * @return string
 */
function saisie_classer_choix_generer_head(string $html_content = '', bool $tester_saisies = false): string {
	include_spip('inc/filtres');
	$flux = '';

	// Pas de saisie alors qu'on veux tester leur présence > hop, on retourne direct
	if ($tester_saisies && strpos($html_content, '<!--!inserer_saisie_editer-->') === false) {
		return $flux;
	}

	// si on a une saisie de type classer_choix, on va charger un script
	if (!$tester_saisies || (strpos($html_content, 'classer_choix') !== false)) {
		$css = timestamp(find_in_path('css/saisie_classer_choix.css'));
		$ins_css = "\n<link rel='stylesheet' href='$css' type='text/css' media='all' />\n";
		$flux .= $ins_css;
		$js = timestamp(find_in_path('prive/javascript/Sortable.js'));
		$flux .= "\n<script type='text/javascript' src='$js'></script>\n";
		$js = produire_fond_statique('javascript/saisie_classer_choix.js');
		$flux .= "\n<script type='text/javascript' src='$js'></script>\n";
	}

	return $flux;
}