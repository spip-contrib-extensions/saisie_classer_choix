# Saisie "classer des choix"

Cette saisie permet de classer une liste de choix par ordre de préférence, par drag'n drop.

TODO : rendre la saisie accessible au clavier en utilisant les rôles ARIA adhoc et un script pour gérer les interactions au clavier, ce qui enlèvera en même temps la dépendance à jQuery UI